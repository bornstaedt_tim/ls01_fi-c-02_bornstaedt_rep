/**
 * @author TimBornstädt
 * @date 16.12.2020
 * 
 * Aufgabe 2
 */
public class UngeradeZahlen {

	public static void main(String[] args) {
		int [] array = new int[10];
		//Hilfsarray mit Wertebereich der ungeraden Zahlen bis 19 | kann auch integer sein mit Wert = 20 ***20 nichtmehr da eh gerade*** 
		int [] hilfsArray = new int[20];
		// j als zähler für array index 
		int j = 0;
		
		for(int i = 0; i < hilfsArray.length && j<array.length; ++i) {
			if(i % 2 != 0) {
				array[j]= i;
				j++;
			}
		}
		
		//Ausgabe Array 
		for(int i=0; i<array.length; ++i) {
			System.out.printf("array[%d] = %d\n", i, array[i]);
		}

	}

}
