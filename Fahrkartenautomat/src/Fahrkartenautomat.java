﻿/**
 * @author	: Tim Bornstädt
 * Klasse	: FI-C-02
 * @date	: 20.01.2021
 * @version : 1.5
 * Aufgabe  : Arbeitsauftrag-LS01-14: Array Implementierung 
 */

import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
    	
       double zuZahlenderBetrag = 0; 
       double rückgabebetrag;
       do {
           zuZahlenderBetrag = fahrkartenbestellungErfassen ();
           rückgabebetrag =  fahrkartenBezahlen( zuZahlenderBetrag );
           fahrkartenAusgeben();
           rueckgeldAusgeben(rückgabebetrag);
       }while(true);
    }
    
    public static double fahrkartenbestellungErfassen () {
    	Scanner tastatur = new Scanner(System.in);
    	
    	double zuZahlenderBetrag = 0;
    	int anzahl, auswahl;
    	String[] fahrkarten ={									// Vorteil, dass man leicht weitere Fahrkarten und preise hinzufügen kann --> möglichkeit aus datei einzulesen und in array zu speichern
    			"Einzelfahrschein Berlin AB",					// Außerdem leichterer Zugriff auf zugehörigen Preis... 
    			"Einzelfahrschein Berlin BC",
    			"Einzelfahrschein Berlin ABC",
    			"Kurzstrecke",
    			"Tageskarte Berlin AB",
    			"Tageskarte Berlin BC",
    			"Tageskarte Berlin ABC",
    			"Kleingruppen-Tageskarte Berlin AB",
    			"Kleingruppen-Tageskarte Berlin BC",
    			"Kleingruppen-Tageskarte Berlin ABC"    			    			
    			};
    	
    	double[] preise = {
    			2.90,
    			3.30,
    			3.60,
    			1.90,
    			8.60,
    			9.00,
    			9.60,
    			23.50,
    			24.30,
    			24.90
    	};
    	
    	if(fahrkarten.length != preise.length) { 
    		System.out.printf("Error at configuration. prices and ticket types are not the same!! For this logic they have to look like arrayTickets[i] => arrayPrices[i] so the Array is sorted\n");
    		System.exit(0);
    	}
    	
    	boolean gate = false;
    	
    	System.out.printf("\n\nWillkommen bei Ihrem Ticketverkäufer Bitte wählen sie ein ticket über das Nummernfeld.\n\n");
    	programmfunktionen(fahrkarten, preise);
    	auswahl = tastatur.nextInt();
    	
    	switch(auswahl) {
    	case 1212: System.out.printf("Willkommen im @Techniker bereich.\n");
    			   System.exit(0);
    			   break;
    	default: System.out.printf("Sie haben sich für %s entschieden. Der Einzelpreis hierfür beträgt: %.2f\n", fahrkarten[auswahl-1], preise[auswahl-1]);
    			 zuZahlenderBetrag = preise[auswahl-1];
    			 break;
    	
    	} 

        //Eingabe der Anzahl der Tickets
        do {
        	 System.out.print("Wie viele Tickets moechten Sie kaufen? (max. 10): ");
             anzahl = tastatur.nextInt();
             if(anzahl > 0 && anzahl <= 10) {
            	 gate = true;
             }
        }while(gate == false);
       
        
        
        zuZahlenderBetrag = zuZahlenderBetrag * anzahl;
    	return zuZahlenderBetrag;
    }
    
    public static double fahrkartenBezahlen( double zuZahlen ) {
    	
    	Scanner tastatur = new Scanner(System.in);
    	double eingezahlterGesamtbetrag = 0.0;
    	double eingeworfeneMünze;
    	double rückgabebetrag;
    	
    	
    	while(eingezahlterGesamtbetrag < zuZahlen){
     	   System.out.printf("Noch zu zahlen: %.2f Euro\n" ,(zuZahlen - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
    	rückgabebetrag = eingezahlterGesamtbetrag - zuZahlen;
    	
    	
    	return rückgabebetrag;
    }
    
    public static void fahrkartenAusgeben() {
    	
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++){
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
    	
    }
    
    public static void rueckgeldAusgeben( double rückgabebetrag) {
        
        if(rückgabebetrag > 0.0){
        	
     	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO " ,rückgabebetrag);
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0){ // 2 EURO-Münzen
 
         	  System.out.println("2 EURO");
 	          rückgabebetrag -= 2.0;
            }
            while(rückgabebetrag >= 1.0){ // 1 EURO-Münzen
            
         	  System.out.println("1 EURO");
 	          rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.5) { // 50 CENT-Münzen
            
         	  System.out.println("50 CENT");
 	          rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.2) { // 20 CENT-Münzen
            
         	  System.out.println("20 CENT");
  	          rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.1){ // 10 CENT-Münzen
 
         	  System.out.println("10 CENT");
 	          rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag >= 0.05){// 5 CENT-Münzen
            
         	  System.out.println("5 CENT");
  	          rückgabebetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.");
    }
    public static void programmfunktionen(String[] fahrkarten, double[] preise) {
    	
    	for(int i=0; i<fahrkarten.length; i++) {
    		System.out.printf("%-35s [%-5.2f]: %d\n", fahrkarten[i], preise[i], i+1);
    	}
    	//System.out.printf("Geheimer-Techniker Code: 1212\n");
    	
    }
}