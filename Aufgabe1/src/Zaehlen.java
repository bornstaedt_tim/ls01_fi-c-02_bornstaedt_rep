/**
 * @author TimBornstädt
 * @date 16.12.2020
 *  Aufgabe 1 Arrayübung
 */
public class Zaehlen {

	public static void main(String[] args) {
		int [] array = new int[10];
		for(int i=0; i<array.length; ++i) {
			array[i] = i;
		}
		for(int i=0;i<array.length; ++i) {
			System.out.printf("Array[%d]: %d \n", i, array[i]);
		}

	}

}
