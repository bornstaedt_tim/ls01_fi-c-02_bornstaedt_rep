/*
 * Name: Tim Bornstädt
 * Datum: 10.03.2021
 * Aufgabe: Benutzerverwaltung
 * Version: 1.0
 */
package benutzerMain;

import benutzer.*;

public class Benutzerverwaltung {

	public static void main(String[] args) {
		BenutzerverwaltungV10.start();
	}

}
