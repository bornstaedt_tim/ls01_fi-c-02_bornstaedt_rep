/*
 * Name: Tim Bornstädt
 * Datum: 10.03.2021
 * Aufgabe: Benutzerverwaltung
 * Version: 1.0
 */

package benutzer;

public class BenutzerListe {
	
	private Benutzer first;
    private Benutzer last;
    public BenutzerListe(){
        first = last = null;
    }
    public void insert(Benutzer b){
        // Sicherheitshalber setzen wir
        // den Nachfolger auf null:
        b.setNext(null);
        if(first == null){
            first = last = b;
        }
        else{
            last.setNext(b);
            last = b;
        }
    }
    public String select(){
        String s = "";
        Benutzer b = first;
        while(b != null){
            s += b.toString() + '\n';
            b = b.getNext();
        }
        return s;
    }
    public String select(String name){
        Benutzer b = first;
        while(b != null){
            if(b.hasName(name)){
                return b.toString();
            }
            b = b.getNext();
        }
        return "";
    }
    public Benutzer sucheBenutzer(String name) {
    	Benutzer b = first;
    	Benutzer a = null;
    	while(b != null){
            if(b.hasName(name)){
                return b;
            }
            b = b.getNext();
        }
    	return a;
        
    }

    public boolean delete(String name){
        // ...
        return true;
    }

}
