/*
 * Name: Tim Bornstädt
 * Datum: 10.03.2021
 * Aufgabe: Benutzerverwaltung
 * Version: 1.0
 */
package benutzer;

import java.util.Scanner;
import java.util.Date;

public class BenutzerverwaltungV10 {
	public static void start(){
		Date date = new Date();
		System.out.println(date);
        BenutzerListe benutzerListe = new BenutzerListe();
        BenutzerListe benutzerListe2 = new BenutzerListe();
        Benutzer nutzer;
        int counter = 0;
        boolean valid = false;
        int key = 0;
        
        
        Scanner tastatur = new Scanner(System.in);
        

        benutzerListe.insert(new Benutzer("Paula", "paula"));
        benutzerListe.insert(new Benutzer("Adam37", "adam37"));
        benutzerListe.insert(new Benutzer("Darko", "darko"));

        // Hier bitte das Menü mit der Auswahl
        //  - Anmelden
        //  - Registrieren
        // einfügen, sowie die entsprechenden Abläufe:
        // Beim Registrieren 2x das Passwort einlesen und vergleichen,
        // das neue Benutzerobjekt erzeugen und in die Liste einfügen.
        // Beim Anmelden (max. 3 Versuche) name und passwort einlesen,
        // in der Liste nach dem Namen suchen und das eingegebene Passwort
        // mit dem gespeicherten vergleichen.
        
        //Menue 
        do {
        	System.out.println("Willkommen in der Benutzerverwaltung.");
        	System.out.println("Was kann ich für sie tun? ");
        	System.out.println("-----------------------------------");
        	System.out.println("Anmelden (1)");
        	System.out.println("Registrieren (2)");
        	System.out.println("-----------------------------------");
        	
        	key = tastatur.nextInt();
        	
        	
        }while(key == 0);
        
        switch(key) {
        case 1:
        	counter = 0;
        	valid = false;
        	
        	do{
        		nutzer = logIn(benutzerListe);
            	if(nutzer == null) {
            		counter += 1;
            		if(counter < 3)	System.out.println("ERROR; Falsches Passwort oder Username. Bitte erneut probieren!");
            		if(counter > 3) System.out.println("ERROR; Falsches Passwort oder Username.");
            	}else {
            		valid = true;
            	}
        	
        	}while(counter < 3 && valid != true);
        	
        	if(valid) {
        		
        		nutzer.setStatus(true);
        		
        		ausgeben(nutzer);
        		
        		nutzer.setLastLogin(date);
        	}      	
        	return;
        case 2:
        	benutzerListe2 = register(benutzerListe); 
        	if(benutzerListe2 == null) {
        		System.out.println("Error bei der Registrierung versuchen sie es erneut!");
        	}else {
        		benutzerListe = benutzerListe2;
        	}
        	return;
        default:
        	return;
        }
        
    }
	public static Benutzer logIn( BenutzerListe benutzerListe) {
		Scanner tastatur = new Scanner(System.in);
		String[] buffer = new String[2];
		Benutzer nutzer;
		Benutzer invalid = null;
		
		System.out.println("Bitte geben Sie ihren Benutzernamen ein:");
    	buffer[0] = tastatur.next();
    	if(buffer[0] == null) {
    		System.out.println("ERROR; READING USERNAME");
    		return invalid;
    	}
    	
    	System.out.println("Bitte geben Sie nun das Passwort ein: ");
    	buffer[1] = tastatur.next();
    	if(buffer[1] == null) {
    		System.out.println("ERROR; READING PASSWORD");
    		return invalid;
    	}
    	nutzer = benutzerListe.sucheBenutzer(buffer[0]);
    	if(nutzer == null) {
    		return invalid;
    	}else if(nutzer.validPassword(buffer[1])) {
    		return nutzer;
    		
    	}
		return invalid;
		
	}
	public static void ausgeben(Benutzer nutzer) {
		System.out.println("Name: " + nutzer.getName());
		System.out.println("Status: " + nutzer.getStatus());
		System.out.println("Last Login: " + nutzer.getDate());

	}
	public static BenutzerListe register( BenutzerListe benutzerListe) {
		Scanner tastatur = new Scanner(System.in);
		String nutzerName;
		String passwd;
		String passwd2;
		
		System.out.println("Bitte geben sie einen eindeutigen Nutzernamen ein: ");
		nutzerName = tastatur.next();
		if(benutzerListe.sucheBenutzer(nutzerName) != null) {
			System.out.println("ERROR! Benutzernamen wird schon verwendet.");
			return null;
		}else {
			System.out.println("Bitte geben sie ein Passwort ein: ");
			passwd = tastatur.next();
			System.out.println("Bitte geben sie erneut das Passwort ein: ");
			passwd2 = tastatur.next();
			
			if(passwd.equals(passwd2)) {
				benutzerListe.insert(new Benutzer(nutzerName, passwd));
				System.out.println("Nutzerkonto erfolgreich angelegt!");
				return benutzerListe;
				
			}else {
				
				System.out.println("ERROR! Passwörter stimmen nicht überein!");
				return null;
			
			}
		}		
		
	}
}

