/*
 * Name: Tim Bornstädt
 * Datum: 10.03.2021
 * Aufgabe: Benutzerverwaltung
 * Version: 1.0
 */
package benutzer;

import java.util.Date;

public class Benutzer {
	
	private String name;
    private String passwort;
    private boolean status;
    private Date lastLogin;

    private Benutzer next;

    public Benutzer(String name, String pw){
        this.name = name;
        this.passwort = pw;

        this.next = null;
    }
    public boolean validPassword(String psswd) {
    	return psswd.equals(this.passwort);
    }

    public boolean hasName(String name){
    	
        return name.equals(this.name);
    }

    public String toString(){
        String s = "";
        s += name + " ";
        s += passwort;
        return s;
    }

    public Benutzer getNext(){
        return next;
    }
    public String getName() {
    	return this.name;
    }
    public void setStatus(boolean stat) {
    	this.status = stat; 
    }
    public boolean getStatus() {
    	return this.status;
    }
    public void setLastLogin(Date date) {
    	this.lastLogin = date;
    }
    public Date getDate() {
    	return this.lastLogin;
    }

    public void setNext(Benutzer b){
        next = b;
    }

}
