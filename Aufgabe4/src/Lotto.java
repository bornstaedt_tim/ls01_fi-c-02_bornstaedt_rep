
public class Lotto {

	public static void main(String[] args) {
		int [] lottoZahlen = {3, 7, 12, 18, 37, 42};
		boolean enthalten12 = false;
		boolean enthalten13 = false;
		
		System.out.printf("{");
		for(int i = 0; i < lottoZahlen.length; i++) {
			System.out.printf("%d ", lottoZahlen[i]);
		}
		System.out.printf("}\n");
		
		for(int i=0; i < lottoZahlen.length; i++) {
			if( lottoZahlen[i] == 12 ) {
				enthalten12 = true;
			}
			if( lottoZahlen[i] == 13) {
				enthalten13 = true;
			}
		}
		if(enthalten12) {
			System.out.printf("12 ist enthalten\n");
		}else {
			System.out.printf("12 ist nicht enthalten\n");
		}
		if(enthalten13) {
			System.out.printf("13 ist enthalten\n");
		}else {
			System.out.printf("13 ist nicht enthalten\n");
		}

	}

}
