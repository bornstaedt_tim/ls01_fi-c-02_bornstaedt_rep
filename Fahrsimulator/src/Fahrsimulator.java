/**
 * @author Tim
 * @version 1.0
 * @date 25.11.2020
 * 
 * Aufgabe: Fahrsimulator
 */
import java.util.Scanner;

public class Fahrsimulator {

	public static void main(String[] args) {
		double v = 0.0; // Geschwindigkeit
		double dv = 0.0; // Beschleunigung in Meter/Sekunde�
		double t = 0.0; 
		
		Scanner scanner = new Scanner(System.in);
		char key = 'a';
		do {
			System.out.printf("Was m�chten sie tun? (q=Quit, w=Beschleunigen): \n");
			key = scanner.next().charAt(0);
			if(key == 'w') {
				System.out.printf("Wie hoch ist die Beschleunigung in Meter/Sekunde^2?: \n");
				dv = scanner.next().charAt(0);
				System.out.printf("Wie lange wird das Auto so Beschleunigt? : \n");
				t = scanner.nextDouble();
				
				v = accelerate(v, dv, t);
				v = v*3.6;
				System.out.printf("Die aktuelle Geschwindigkeit des Autos ist: %f km/h \n", v);
			}
		}while (key != 'q');
		scanner.close();
		return;
	}
	
	//Beschleunigungsmethode
	public static double accelerate(double v, double dv, double t) {
		double v0 = v;
		
		v = (dv*t) + v0;
		
		return v;
	}

}
