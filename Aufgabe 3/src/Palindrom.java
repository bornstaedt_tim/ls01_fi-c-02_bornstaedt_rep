/**
 * @author TimBo
 * @date 16.12.2020 
 * Aufgabe 3 'Palindrom'
 *  
 */
import java.util.Scanner;

public class Palindrom {

	public static void main(String[] args) {
		
		Scanner eingabe = new Scanner(System.in);
		//Array für fünf Integereingaben;
		char[] array = new char[5];
		
		//Hilfsvariable für do .. while Schleife
		int n = 0;
		
		//Eingabe der Zahlen
		do {
			System.out.println("Bitte geben Sie ein Zeichen ein: ");
			array[n] = eingabe.next().charAt(0);
			++n;
		}while(n < array.length);
		
		//Ausgabe in umgekehrter Reihenfolge
		for(int i = array.length-1; i>=0; --i) {
			System.out.println("Array[" + i + "] = " + array[i]);
		}
		
		eingabe.close();
	}

}
