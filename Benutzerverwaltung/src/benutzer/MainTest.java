/**
 * @author	: Tim Bornstädt
 * Klasse	: FI-C-02
 * @date	: 17.02.2021
 * @version : 1.5
 * Aufgabe  : OOP Benutzerverwaltung Aufgaben 1-3 
 */
package benutzer;
import java.util.Scanner;

public class MainTest {

	public static void main(String[] args) {
		
		Scanner keyboard = new Scanner(System.in);
		
		String name;
		String email;
		String passwort;
		int berechtigung;
		char isOn;
		boolean isOnline;
		
		Benutzer user01 = new Benutzer();
		Benutzer user02 = new Benutzer();
		
		System.out.println("Bitte geben Sie den Namen des ersten Benutzers ein: ");
		name = keyboard.next();
		//System.out.println(name);
		user01.setName(name);
		//System.out.println("Im Benutzer: " + user01.name);
		System.out.println("Bitte geben Sie nun die E-Mail des ersten Benutzers ein: ");
		email = keyboard.next();
		//System.out.println(email);
		user01.setEmail(email);
		//System.out.println("Im Benutzer: " + user01.email);
		System.out.println("Bitte legen sie nun ein Passwort fest: ");
		passwort = keyboard.next();
		user01.setPassword(passwort);
		//System.out.println(user01.psswd);
		user01.setPassword(passwort);
		//System.out.println("Im Benutzer: " + user01.psswd);
		System.out.println("Mit welchen Rechten soll der User ausgestattet werden? 0=admin 1=useradmin 2=user 3=subscriber");
		berechtigung = keyboard.nextInt();
		//System.out.println(berechtigung);
		user01.setBerechtigung(berechtigung);
		//System.out.println("Im Benutzer: " + user01.berechtigung);
		System.out.println("Ist der User gerade Online?: (y)Yes (n)No");
		isOn = keyboard.next().charAt(0);
		//System.out.println(isOn);
		if(isOn == 'y') {
			isOnline = true;
		}else {
			isOnline = false;
		}
		user01.setIsOnline(isOnline);
		//System.out.println("Im Benutzer: " + user01.isOnline);
		
		
		System.out.println("Bitte geben Sie den Namen des zweiten Benutzers ein:");
		name = keyboard.next();
		//System.out.println(name);
		user02.setName(name);
		//System.out.println("Im Benutzer: " + user02.name);
		System.out.println("Bitte geben Sie nun die E-Mail-Adresse an: ");
		email = keyboard.next();
		//System.out.println(email);
		user02.setEmail(email);
		//System.out.println("Im Benutzer: " + user02.email);
		System.out.println("Bitte legen sie nun ein Passwort fest: ");
		passwort = keyboard.next();
		user02.setPassword(passwort);
		//System.out.println(user01.psswd);
		user02.setPassword(passwort);
		//System.out.println("Im Benutzer: " + user02.psswd);
		System.out.println("Mit welchen Rechten soll der User ausgestattet werden? 0=admin 1=useradmin 2=user 3=subscriber");
		berechtigung = keyboard.nextInt();
		//System.out.println(berechtigung);
		user02.setBerechtigung(berechtigung);
		//System.out.println("Im Benutzer: " + user02.berechtigung);
		System.out.println("Ist der User gerade Online?: (y)Yes (n)No");
		isOn = keyboard.next().charAt(0);
		//System.out.println(isOn);
		if(isOn == 'y') {
			isOnline = true;
		}else {
			isOnline = false;
		}
		user02.setIsOnline(isOnline);
		//System.out.println("Im Benutzer: " + user02.isOnline);
		
		//testing get functions
		System.out.println("Name: " + user01.getName() +" E-Mail: " + user01.getEmail() +" Passwort: " + user01.getPassword() +" Berechtigungen: " + user01.getBerechtigung() +" Online: " + user01.getIsOnline());
		System.out.println("Name: " + user02.getName() +" E-Mail: " + user02.getEmail() +" Passwort: " + user02.getPassword() +" Berechtigungen: " + user02.getBerechtigung() +" Online: " + user02.getIsOnline());
		
		
	}

}
