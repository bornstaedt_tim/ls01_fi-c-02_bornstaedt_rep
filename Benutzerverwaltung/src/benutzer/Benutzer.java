/* Name: Tim Bornstädt
 * Datum: 10.03.2021
 * Aufgabe: Benutzerverwaltung
 * Version: 1.0
 */

package benutzer;

public class Benutzer {
	String name;
	String email;
	String psswd;
	int berechtigung;
	boolean isOnline;
	
	
	//set function for class Benutzer
	public void setName(final String name) {
		this.name = name;
	}
	
	public void setEmail(final String email) {
		this.email = email;
	}
	
	public void setPassword(final String psswd) {
		this.psswd = psswd;
	}
	
	public void setBerechtigung(final int berechtigung) {
		this.berechtigung = berechtigung;
	}
	
	public void setIsOnline(final boolean isOnline) {
		this.isOnline = isOnline;
	}
	
	//get function for class Benutzer
	public String getName() {
		return this.name;
	}
	
	public String getEmail() {
		return this.email;
	}
	
	public String getPassword() {
		return this.psswd;
	}
	
	public int getBerechtigung() {
		return this.berechtigung;
	}
	
	public boolean getIsOnline() {
		return this.isOnline;
	}
	
}
