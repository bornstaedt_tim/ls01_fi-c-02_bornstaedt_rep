/**
 * @author Tim
 * @version 1.0
 * @Datum 25.11.2020
 * 
 * Aufgabe : Schaltjahr
 */
import java.util.Scanner;


public class Schaltjahr {

	public static void main(String[] args) {
		int jahr;
		Scanner myInputScanner = new Scanner(System.in);
		System.out.printf("Bitte gebne Sie eine Jahreszahl ein: ");
		jahr = myInputScanner.nextInt();
		
		if(jahr % 4 == 0 && jahr > 45) {
			if(jahr % 100 == 0) {
				if(jahr % 400 == 0) {
					System.out.printf("Das Jahr %d ist ein Schaltjahr.", jahr);
					myInputScanner.close();
					return;
				}
				System.out.printf("Das Jahr %d ist kein Schaltjahr.", jahr);
				myInputScanner.close();
				return;
			}
			System.out.printf("Das Jahr %d ist ein Schaltjahr.", jahr);
			myInputScanner.close();
			return;
		}else {System.out.printf("Das Jahr %d ist kein Schaltjahr.\n", jahr);}
		
		myInputScanner.close();
		return;
		

	}

}
